# EvaLatin2020

This repository documents a gradient boosting-Seq2Seq system 
for POS tagging and lemmatization of Latin developed for the EvaLatin 
shared task 2020 [1].

For more details, refer to the forthcoming article [2]:

Celano, Giuseppe G. A., A Gradient Boosting-Seq2Seq System for Latin 
POS Tagging and Lemmatization. In Rachele Sprugnoli et al. (eds.), 
*Proceedings of the LT4HALA 2020 Workshop - 1st Workshop on Language 
Technologies for Historical and Ancient Languages*, satellite event to the 
Twelfth International Conference on Language Resources and Evaluation
(LREC 2020), Paris, France, May. European Language Resources Association (ELRA).

-----
[1] https://github.com/CIRCSE/LT4HALA <br/>
[2] https://git.informatik.uni-leipzig.de/celano/evalatin2020/-/blob/master/preprint.pdf (preprint)
