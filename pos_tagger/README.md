# POS tagger

The POS tagger has been trained using LightGBM [1]. The model can be loaded in
Python using the command `lightgbm.Booster(model_file="directory")`. The text to 
postag needs to be vectorized in the way `train.py` documents. 

-----
[1] https://lightgbm.readthedocs.io/en/latest/index.html