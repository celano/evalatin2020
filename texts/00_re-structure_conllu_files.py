# Python 3.8.0
# Author: Giuseppe G. A. Celano

import os
import pandas as pd
import numpy as np
import json

np.random.seed(3)

def retrieve_file_names(path):
    return sorted(os.listdir(path))

def extract_text(path, file_name):
        full_path = path + file_name
        with open(full_path, "rt") as myf:
           text = myf.read()
        d = {}
        d['fileid'] = file_name
        d['text'] = text
        return d

def sentence_split(text, file_name):
    text_as_sentences = []
    for s1 in text.split("\n\n")[:-1]:
        s2 = {}
        s2['fileid'] = file_name
        s2['sentence'] = s1
        text_as_sentences.append(s2)
    return text_as_sentences

# the following is not needed
def shuffle_sentences_in_text(text):
    sh = np.random.shuffle(text)
    return text                      # change in place

def split_text(text):
        a = np.split(text, [int(0.8*len(text)), int(0.9*len(text))]) # list containing three ndarray for the split 80%, 10%, 10%
        return a

def tokenize(text):
        train_or_dev_or_test = [] 
        for s in text:
            ppp = s['sentence'].split('\n')
            for n, t in enumerate(ppp):
                p0 = ppp[0].split(" ")[-1]
                p1 = ppp[1].split("# text = ")[1]
                if n > 1:
                    token = {}
                    token['fileid'] = s['fileid']
                    token['sentid'] = p0
                    token['text'] = p1
                    sd = t.split("\t")
                    token['position'] = sd[0]
                    token['form'] = sd[1]
                    token['lemma'] = sd[2]
                    token['postag'] = sd[3]
                    train_or_dev_or_test.append(token)
        return train_or_dev_or_test

def merge_authors(all_texts):
    train_all = []
    dev_all = []
    test_all = []
    for t in all_texts:
        train_all.append(t[0])
        dev_all.append(t[1])
        test_all.append(t[2])
    a = [y for x in train_all for y in x]
    b = [y for x in dev_all for y in x]
    c = [y for x in test_all for y in x]
    return [a,b,c]

def build_final_dataframe(p):
    split = []
    for t in p:
        df = pd.DataFrame.from_dict(t)
        split.append(df)
    return split

path = "/Users/technician/Documents/lt4hala/LT4HALA-master/" \
           "data_and_doc/training_data_10-12-2019/"

file_names = retrieve_file_names(path)
# the following is the sorted order:
#['Caesar_BellumCivile_LiberII.conllu',
# 'Caesar_BellumGallicum.conllu',
# 'Cicero_Philippica.conllu',
# 'Pliny_Younger_Epistulae_1-8.conllu',
# 'Seneca_DeBeneficiis.conllu',
# 'Seneca_DeClementia.conllu',  
# 'Tacitus_Historiae.conllu']
    
all_texts = []
    
for t in file_names:
    a = extract_text(path, t)
    b = sentence_split(a['text'], a['fileid'])
    ###c = shuffle_sentences_in_text(b) it is not needed
    d = split_text(b)
    p = []
    for tr_dev_te in d:
        kk = tokenize(tr_dev_te)
        p.append(kk)     
    all_texts.append(p)
   
# all_texts[0][1] gives access to first text (there are 7) and dev split (there 3 splits)
# sentences are shuffled within a split

final_split = merge_authors(all_texts)
 
# the following code is to create train, dev, test files
#with open('/Users/technician/Documents/lt4hala/myscrips/train.json', "w") as f:
#    json.dump(final_split[0], f, indent=3)
#with open('/Users/technician/Documents/lt4hala/myscrips/dev.json', "w") as f:
#    json.dump(final_split[1], f, indent=3)
#with open('/Users/technician/Documents/lt4hala/myscrips/test.json', "w") as f:
#    json.dump(final_split[2], f, indent=3)