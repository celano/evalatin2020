# Texts

The original conllu files released for the shared task [1] have been converted 
into json. The folder contains the files 
(`train.json.zip`, `dev.json`, and `test.json`) and the code to generate them
(`00_re-structure_conllu_files.py`).

-----
[1] https://github.com/CIRCSE/LT4HALA/tree/master/data_and_doc