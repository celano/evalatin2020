# Word Embeddings

The folder contains the word embeddings model created using FastText [1]. The parameters
used are `"skipgram"`, `minn=2`, `maxn=5`, `dim=300`, and `lr=0.03`.

-----
[1] https://fasttext.cc/
